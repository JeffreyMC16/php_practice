<?php

use Core\App;

$db = App::resolve('Core/Database');

$currenUserId = 1;

$note = $db->query('select * from notes where id = :id', ['id' => $_GET['id']])->findOrFail();



view("notes/edit.view.php", [
    'heading' => 'Edit note',
    'errors' => [],
    'note' => $note
]);