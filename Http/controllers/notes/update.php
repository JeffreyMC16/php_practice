<?php

use Core\App;
use Core\Database;
use Core\Validator;


//find the id of the note
$db = App::resolve('Core/Database');

$currenUserId = 1;

$note = $db->query('select * from notes where id = :id', ['id' => $_POST['id']])->findOrFail();

//check whether teh user can update the note or not
authorize($note['user_id'] === $currenUserId);

//validate the form
$errors = [];

if (!Validator::string($_POST['body'], 1, 100)) {
    $errors['body'] = 'A body of no more than 100 characters is required';
}

if(count($errors)){
    /** @noinspection PhpVoidFunctionResultUsedInspection */
    return view('notes/edit.view.php', [
        'heading' => 'Edit note',
        'errors' => $errors,
        'note' => $note
    ]);
}

$db->query('update notes set body = :body where id = :id', [
    'id' => $_POST['id'],
    'body' => $_POST['body']
]);

//redirect to user
header('location: /notes');
exit();
