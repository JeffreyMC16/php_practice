<?php

use Core\Validator;

$email = $_POST['email'];
$password = $_POST['password'];


$errors = [];
//Validate the inputs
if(! Validator::email($email)){
    $errors['email'] = 'Please provide a valid email address';
}

if(! Validator::string($password, 7, 255)){
    $errors['password'] = 'Please provide a password at least of seven characters';
}

if(! empty($errors)){
    return view('registration/create.view.php', [
        'errors' => $errors
    ]);
}

//check if the account already exists
$db = \Core\App::resolve('Core/Database');
$user = $db->query('select * from users where email = :email', [
    'email' => $email
])->find();


//if user exists
if($user){
    header('location: /');
    exit();
}else{
    //save user on the database, log the user in and redirect
    $db->query('INSERT INTO users(email, password) VALUES (:email, :password)', [
        'email' => $email,
        'password' => password_hash($password, PASSWORD_BCRYPT)
    ]);

    login([
        'email' => $email
    ]);

    header('location: /');
    exit();
}

