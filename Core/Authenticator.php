<?php

namespace Core;

class Authenticator
{
    public function attempt($email, $password)
    {
        //log in the user if the credentials match

        $user = App::resolve('Core/Database')->query('select * from users where email = :email', [
            'email' => $email
        ])->find();

        if($user){
            //we have a user but we don't know if the pass matches
            if(password_verify($password, $user['password'])){
                $this->login([
                    'email' => $email
                ]);

                return true;
            }
        }

        return false;
    }


    public function login($user)
    {
        //mark that the user has logged in
        $_SESSION['user'] = [
            'email' => $user['email']
        ];

        session_regenerate_id(true);
    }

    public function logout()
    {
       Session::destroy();
    }
}